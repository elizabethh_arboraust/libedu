<?php

$date = 'j M Y';
$time = 'h:i A';

return [

    /*
    |--------------------------------------------------------------------------
    | Year Format
    |--------------------------------------------------------------------------
    |
    | This value determines the default year format
    | e.g. 2019
    |
    */

    'year' => 'Y',

    /*
    |--------------------------------------------------------------------------
    | Month Format
    |--------------------------------------------------------------------------
    |
    | This value determines the default month format
    | e.g. 09
    |
    */

    'month' => 'm',

    /*
    |--------------------------------------------------------------------------
    | Date Format
    |--------------------------------------------------------------------------
    |
    | This value determines the default date format
    | e.g. 16
    |
    */

    'date' => 'd',

    /*
    |--------------------------------------------------------------------------
    | Date Format
    |--------------------------------------------------------------------------
    |
    | This value determines the default date format YYYY-MM-DD mostly for data storage and transfer
    | e.g. 2019-09-16
    |
    */

    'date_format' => 'Y-m-d',

    /*
    |--------------------------------------------------------------------------
    | DateTime Format
    |--------------------------------------------------------------------------
    |
    | This value determines the default datetime format YYYY-MM-DD hh:mm:ss mostly for data storage and transfer
    | e.g. 2019-09-16 13:23:05
    |
    */

    'datetime_format' => 'Y-m-d H:i:s',

    /*
    |--------------------------------------------------------------------------
    | Readable day date-month-year format
    |--------------------------------------------------------------------------
    |
    | This value determines the default readable format for user display
    | e.g. Friday, 3 Jan 2020
    |
    */

    'readable_day_date' => "l, $date",

    /*
    |--------------------------------------------------------------------------
    | Readable day date-month-year format
    |--------------------------------------------------------------------------
    |
    | This value determines the default readable format for user display
    | e.g. Friday, 3 Jan 2020
    |
    */

    'readable_day_datetime' => "l, $date $time",

    /*
    |--------------------------------------------------------------------------
    | Readable datetime format
    |--------------------------------------------------------------------------
    |
    | This value determines the default readable format for user display
    | e.g. 3 Jan 2020 07:00PM
    |
    */

    'readable_datetime' => "$date $time",

    /*
    |--------------------------------------------------------------------------
    | Readable date format
    |--------------------------------------------------------------------------
    |
    | This value determines the default readable format for user display
    | e.g. 3 Jan 2020 07:00PM
    |
    */

    'readable_date' => $date,

    /*
    |--------------------------------------------------------------------------
    | Readable time format
    |--------------------------------------------------------------------------
    |
    | This value determines the default readable format for user display
    | e.g. 07:00PM
    |
    */

    'readable_time' => $time,
];
