<?php

return [

    /*
    |--------------------------------------------------------------------------
    | JWT iss
    |--------------------------------------------------------------------------
    |
    | The iss (issuer) claim identifies the principal that issued the JWT.
    |
    */

    'iss' => env('JWT_ISS', 'libEdu'),

    /*
    |--------------------------------------------------------------------------
    | JWT aud
    |--------------------------------------------------------------------------
    |
    | The aud (audience) claim identifies the recipients that the JWT is intended for.
    | Replace this with the subscriber's code from db.
    |
    */

    'aud' => 'subscriber',

    /*
    |--------------------------------------------------------------------------
    | JWT iat
    |--------------------------------------------------------------------------
    |
    | The iat (issued at) claim identifies the time at which the JWT was issued.
    | Replace this with current unix time e.g. by calling time() function
    |
    */

    'iat' => time(),

    /*
    |--------------------------------------------------------------------------
    | JWT exp
    |--------------------------------------------------------------------------
    |
    | The exp (issued at) claim identifies the expiration time on or after which
    |   the JWT MUST NOT be accepted for processing.
    |
    */

    'exp' => time() + env('JWT_EXP', 300), // 5 minutes expiration

    /*
    |--------------------------------------------------------------------------
    | Total second until JWT expiry
    |--------------------------------------------------------------------------
    */

    'exp_second' => env('JWT_EXP', 300), // 5 minutes expiration

    /*
    |--------------------------------------------------------------------------
    | JWT nbf
    |--------------------------------------------------------------------------
    |
    | The nbf (not before) claim identifies the time before which the JWT
    | MUST NOT be accepted for processing.
    | Replace this with the time this app goes live or testing with client
    |
    */

    'nbf' => strtotime(env('JWT_NBF', '2020-01-01') . ' ' . env('APP_TIMEZONE')),
];
