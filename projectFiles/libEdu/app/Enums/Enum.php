<?php

namespace App\Enums;

use App\Api\ErrorCode;
use App\Api\ErrorMessage;
use App\Exceptions\CustomException;
use ReflectionClass;
use ReflectionException;

class Enum
{
    protected static $constCacheArray = NULL;

    /**
     * Get all the constants in this class.
     *
     * @return array
     * @throws CustomException
     */
    public static function getConstants()
    {
        try
        {
            $className = get_called_class();
            $constants = &self::$constCacheArray[$className];

            if ($constants)
            {
                return $constants;
            }

            $reflector = new ReflectionClass($className);
            $constants = $reflector->getConstants();

            return $constants;
        }
        catch (ReflectionException $exception)
        {
            throw new CustomException(ErrorMessage::ERROR_GETTING_CONSTANTS, ErrorCode::ERROR_GETTING_CONSTANTS, $exception);
        }
    }

    /**
     * Check if the value is a valid enum.
     *
     * @param mixed $value
     * @return bool
     * @throws CustomException
     */
    public static function isValid($value)
    {
        foreach (self::getConstants() as $constant)
        {
            if ($value === $constant)
            {
                return true;
            }
        }

        return false;
    }
}