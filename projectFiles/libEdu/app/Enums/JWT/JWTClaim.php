<?php

namespace App\Enums\JWT;

/**
 * A list of claims available for JWT payload.
 *
 * @package App\Enums
 */
class JWTClaim
{
    const ISSUER = 'iss';
    const AUDIENCE = 'aud';
    const ISSUED_AT_TIME = 'iat';
    const EXPIRATION_TIME = 'exp';
    const NOT_BEFORE = 'nbf';
}
