<?php

namespace App\Enums\Messages\Errors;

/**
 * JWT related error messages.
 */
class JWTErrorMessage
{
    const MUST_BE_AFTER = 'JWT must be after';
    const EXPIRED = 'JWT has expired';
    const JWT_AUTH_FAIL = 'JWT authentication failed';
    const JWT_NOT_FOUND = 'JWT not found';
    const JWT_NOT_FOUND_FROM_LONGBOW_APP = 'JWT not found from Longbow app';
    const IAT_CLAIM_IS_REQUIRED = 'IAT claim is required';
    const NO_AUTHORIZATION_HEADER = 'Authorization header not found';
    const NULL_JWT_SECRET = 'JWT Secret is null';
    const USER_NAME_NOT_FOUND = 'userName not found in JWT';
}
