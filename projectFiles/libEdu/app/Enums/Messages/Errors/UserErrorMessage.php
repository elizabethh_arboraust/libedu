<?php

namespace App\Enums\Messages\Errors;

class UserErrorMessage
{
    const INVALID_LOGIN = 'Invalid user or password';
    const USER_NOT_FOUND = 'User not found';
}
