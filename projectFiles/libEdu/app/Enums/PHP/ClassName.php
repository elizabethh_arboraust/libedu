<?php

namespace App\Enums\PHP;

/**
 * A list of class names that are required to be compared in the code.
 *
 * @package App\Enums
 */
class ClassName
{
    const APP_EXCEPTIONS_CUSTOM_EXCEPTION = 'App\Exceptions\CustomException';
    const ILLUMINATE_DATABASE_ELOQUENT_COLLECTION = 'Illuminate\Database\Eloquent\Collection';
    const MODELS = 'models';
    const SYMFONY_COMPONENT_HTTPFOUNDATION_RESPONSE = 'Symfony\Component\HttpFoundation\Response';
}
