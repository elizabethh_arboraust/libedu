<?php

namespace App\Enums\PHP;

class PHPDataType
{
    const OBJECT = 'object';
    const STD_CLASS = 'stdClass';
}
