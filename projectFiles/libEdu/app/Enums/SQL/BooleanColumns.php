<?php

namespace App\Enums\SQL;

use App\Enums\Enum;

class BooleanColumns extends Enum
{
    const IS_ACTIVE = 'isActive';
}
