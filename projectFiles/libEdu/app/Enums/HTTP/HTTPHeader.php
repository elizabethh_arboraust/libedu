<?php

namespace App\Enums\HTTP;

class HTTPHeader
{
    const ACCEPT = 'Accept';
    const AUTHORIZATION = 'Authorization';
    const CONTENT_TYPE = 'Content-Type';
    const CONTENT_DISPOSITION = 'Content-Disposition';
}
