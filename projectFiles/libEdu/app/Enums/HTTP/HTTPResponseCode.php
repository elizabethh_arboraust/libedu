<?php

namespace App\Enums\HTTP;

class HTTPResponseCode
{
    const OK = 200;
    const NO_CONTENT = 204;
    const BAD_REQUEST = 400;
    const FORBIDDEN = 403;
    const METHOD_NOT_ALLOWED = 405;
}
