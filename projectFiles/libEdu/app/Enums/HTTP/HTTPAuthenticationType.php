<?php

namespace App\Enums\HTTP;

class HTTPAuthenticationType
{
    const BEARER = 'Bearer';
}
