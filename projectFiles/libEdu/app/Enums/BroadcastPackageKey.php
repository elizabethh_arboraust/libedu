<?php

namespace App\Enums;

/**
 * These keys are case-sensitive.
 *
 * @package App\Enums
 */
class BroadcastPackageKey extends Enum
{
    const FIRST_NAME = 'firstName';
    const MIDDLE_NAME = 'middleName';
    const LAST_NAME = 'lastName';
    const USER_NAME = 'userName';
    const USER_PASSWORD = 'userPassword';
    const USER_PASSWORD_ENCRYPTED = 'userPasswordEncrypted';
    const USER_RFID = 'userRFID';
    const USER_FINGER_PRINT = 'userFPrint';
    const BIRTH_DATE = 'birthDate';
    const GENDER = 'gender';
    const PERSON_TYPE = 'personType';

    // Used when we broadcast data in human language (Y/N) instead of machine language (1/0).
    const IS_ACTIVE = 'isActive';
    const ACTIVE_STATUS = 'activeStatus';

    const LAST_OIMS_LOCATION_UPDATE_AT = 'lastOIMSLocationUpdateAt';
    const LAST_ONBOARDING_AT = 'lastOnBoardingAt';
    const BROADCAST_AT = 'broadcastAt';

    const AGENCY_LOCATION = 'agencyLocation';
    const AGENCY_LOCATION_ID = 'agencyLocationID';
    const HOUSING_LOCATION = 'housingLocation';
    const HOUSING_LOCATION_ID = 'housingLocationID';
    const LEVEL_1_CODE = 'level1Code';
    const LEVEL_2_CODE = 'level2Code';
    const LEVEL_3_CODE = 'level3Code';
    const LEVEL_4_CODE = 'level4Code';
}
