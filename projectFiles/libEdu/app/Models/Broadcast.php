<?php

namespace App\Models;

use App\Enums\BroadcastPackageKey;

class Broadcast extends Model
{
    public static function saveData($post)
    {
        $broadcast = new self;
        $broadcast->userName = $post[BroadcastPackageKey::USER_NAME];
        $broadcast->data = json_encode($post);
        $broadcast->save();

        return $broadcast->ID;
    }

    public static function deleteData($id)
    {
        self::destroy($id);
    }
}