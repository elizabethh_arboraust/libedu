<?php

namespace App\Models;

use App\Enums\BroadcastPackageKey;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class User extends Model
{
    protected static $limit = 1;

    protected $primaryKey = 'userID';
    protected $fillable =
        [
            BroadcastPackageKey::USER_NAME,
            BroadcastPackageKey::FIRST_NAME,
            BroadcastPackageKey::MIDDLE_NAME,
            BroadcastPackageKey::LAST_NAME,
            BroadcastPackageKey::USER_PASSWORD,
            BroadcastPackageKey::USER_PASSWORD_ENCRYPTED,
            BroadcastPackageKey::USER_RFID,
            BroadcastPackageKey::BIRTH_DATE,
            BroadcastPackageKey::GENDER,
            BroadcastPackageKey::PERSON_TYPE,
            BroadcastPackageKey::IS_ACTIVE,
            BroadcastPackageKey::LAST_OIMS_LOCATION_UPDATE_AT,
            BroadcastPackageKey::LAST_ONBOARDING_AT,
            BroadcastPackageKey::AGENCY_LOCATION,
            BroadcastPackageKey::AGENCY_LOCATION_ID,
            BroadcastPackageKey::HOUSING_LOCATION,
            BroadcastPackageKey::HOUSING_LOCATION_ID,
            BroadcastPackageKey::LEVEL_1_CODE,
            BroadcastPackageKey::LEVEL_2_CODE,
            BroadcastPackageKey::LEVEL_3_CODE,
            BroadcastPackageKey::LEVEL_4_CODE,
        ];

    private static $MOODLE_CONNECTION = 'moodle';

    /**
     * Onboard/offboard depending on 'ActiveStatus'
     *
     * @param $post
     */
    public static function onboard($post)
    {
        $condition =
            [
                BroadcastPackageKey::USER_NAME => $post[BroadcastPackageKey::USER_NAME],
            ];

        $password = $post[BroadcastPackageKey::USER_PASSWORD];
        $userPasswordHashed = Hash::make($password);
        $userPasswordEncrypted = encrypt($password);

        $update =
            [
                BroadcastPackageKey::FIRST_NAME => $post[BroadcastPackageKey::FIRST_NAME],
                BroadcastPackageKey::MIDDLE_NAME => $post[BroadcastPackageKey::MIDDLE_NAME],
                BroadcastPackageKey::LAST_NAME => $post[BroadcastPackageKey::LAST_NAME],
                BroadcastPackageKey::USER_PASSWORD => $userPasswordHashed,
                BroadcastPackageKey::USER_PASSWORD_ENCRYPTED => $userPasswordEncrypted,
                BroadcastPackageKey::USER_RFID => $post[BroadcastPackageKey::USER_RFID],
                BroadcastPackageKey::BIRTH_DATE => $post[BroadcastPackageKey::BIRTH_DATE],
                BroadcastPackageKey::GENDER => $post[BroadcastPackageKey::GENDER],
                BroadcastPackageKey::PERSON_TYPE => $post[BroadcastPackageKey::PERSON_TYPE],
                BroadcastPackageKey::IS_ACTIVE => self::getIsActive($post[BroadcastPackageKey::ACTIVE_STATUS]),
                BroadcastPackageKey::LAST_OIMS_LOCATION_UPDATE_AT => $post[BroadcastPackageKey::LAST_OIMS_LOCATION_UPDATE_AT],
                BroadcastPackageKey::LAST_ONBOARDING_AT => $post[BroadcastPackageKey::LAST_ONBOARDING_AT],
                BroadcastPackageKey::BROADCAST_AT => $post[BroadcastPackageKey::BROADCAST_AT],
                BroadcastPackageKey::AGENCY_LOCATION => $post[BroadcastPackageKey::AGENCY_LOCATION],
                BroadcastPackageKey::AGENCY_LOCATION_ID => $post[BroadcastPackageKey::AGENCY_LOCATION_ID],
                BroadcastPackageKey::HOUSING_LOCATION => $post[BroadcastPackageKey::HOUSING_LOCATION],
                BroadcastPackageKey::HOUSING_LOCATION_ID => $post[BroadcastPackageKey::HOUSING_LOCATION_ID],
                BroadcastPackageKey::LEVEL_1_CODE => $post[BroadcastPackageKey::LEVEL_1_CODE],
                BroadcastPackageKey::LEVEL_2_CODE => $post[BroadcastPackageKey::LEVEL_2_CODE],
                BroadcastPackageKey::LEVEL_3_CODE => $post[BroadcastPackageKey::LEVEL_3_CODE],
                BroadcastPackageKey::LEVEL_4_CODE => $post[BroadcastPackageKey::LEVEL_4_CODE],
            ];

        self::updateOrCreate($condition, $update);
    }

    /**
     * Onboard/offboard depending on 'ActiveStatus'
     *
     * @param $post
     */
    public static function onboardToMoodle($post)
    {
        $isActive = self::getIsActive($post[BroadcastPackageKey::ACTIVE_STATUS]);
        $condition =
            [
                'username' => $post[BroadcastPackageKey::USER_NAME],
            ];

        $password = $post[BroadcastPackageKey::USER_PASSWORD];
        $userPasswordHashed = password_hash($password, PASSWORD_DEFAULT);

        $update =
            [
                'password' => $isActive ? $userPasswordHashed : '',
                'confirmed' => 1,
                'mnethostid' => 1,
                'firstname' => $post[BroadcastPackageKey::FIRST_NAME],
                'lastname' => $post[BroadcastPackageKey::LAST_NAME],
                'deleted' => !$isActive,
            ];

        DB::connection(self::$MOODLE_CONNECTION)->table('mdl_user')->updateOrInsert($condition, $update);
    }

    public static function getUser($userName)
    {
        $userNameKey = BroadcastPackageKey::USER_NAME;

        $sql = <<<SQL
            SELECT
                userID
                ,userName
                ,userPasswordEncrypted
            FROM users
            WHERE
                userName = :$userNameKey
                AND isActive = true
SQL;

        $bindings =
            [
                $userNameKey => $userName,
            ];

        return self::getFirstResult(self::getBySQL($sql, $bindings));
    }

    private static function getIsActive($activeStatus)
    {
        return $activeStatus === 'Y';
    }
}