<?php

namespace App\Models;

use App\Enums\PHP\ClassName;
use App\Enums\SQL\BooleanColumns;
use App\Exceptions\CustomException;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Facades\DB;

class Model extends EloquentModel
{
    public $timestamps = false;

    protected static $limit = null;
    protected static $defaultSelect = ['*'];
    protected static $booleanColumns = [];

    protected $primaryKey = 'ID';

    /**
     * Get data from a specific table. Values from BIT Data Type are modified to BOOLEAN.
     *
     * @param array $where a two dimensional array e.g. [['userName', 'JimboB'], ['isActive', 1]]
     * @param array $select the column names
     * @param array $orderBy a list of orders e.g. [['ID ASC'], ['isActive DESC'], ['updatedAt']]
     * @param bool $isLimit true if we're going to limit this query, false not to limit.
     * @param int $limit the limit for this query
     * @return mixed
     * @throws CustomException
     */
    public static function get(array $where = [], array $select = [], $orderBy = [], bool $isLimit = true, int $limit = null)
    {
        $result = self::getUnmodified($where, $select, $orderBy, $isLimit, $limit);

        return Model::modifyBitToBooleanInResult($result);
    }

    /**
     * Get data from a specific table.
     *
     * @param array $where a two dimensional array e.g. [['userName', 'JimboB'], ['isActive', 1]]
     * @param array $select the column names
     * @param array $orderBy a list of orders e.g. [['ID ASC'], ['isActive DESC'], ['updatedAt']]
     * @param bool $isLimit true if we're going to limit this query, false not to limit.
     * @param int $limit the limit for this query
     * @return mixed
     * @throws CustomException
     */
    public static function getUnmodified(array $where = [], array $select = [], $orderBy = [], bool $isLimit = true, int $limit = null)
    {
        try
        {
            return self::getQuery($where, $select, $orderBy, $isLimit, $limit)->get();
        }
        catch (Exception $exception)
        {
            throw new CustomException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }


    public static function getQuery(array $where = [], array $select = [], $orderBy = [], bool $isLimit = true, int $limit = null)
    {
        $className = get_called_class();
        $select = $select ? $select : $className::$defaultSelect;
        $query = $className::where($where);

        if ($select)
        {
            $query->select($select);
        }

        if ($orderBy)
        {
            $orderBy = DB::raw(implode(', ', $orderBy));
            $query->orderByRaw($orderBy);
        }

        if ($isLimit)
        {
            $limit = $limit ? $limit : $className::$limit;
            $query->limit($limit);
        }

        return $query;
    }

    /**
     * Modifies int php data type (from bit(1) mysql data type) to bool data type.
     *
     * @param mixed|null $result
     * @param array $bitColumns this can be used to passed in aliases or when more than one tables are used in the query
     *      (because of "describe <table_name>" statement that we run to get data type of a table)
     * @return mixed|null
     */
    public static function modifyBitToBooleanInResult($result = null, array $bitColumns = [])
    {
        if (!$result)
        {
            return null;
        }

        try
        {
            $collectionClass = get_class($result);
        }
        catch (Exception $exception)
        {
            $collectionClass = null;
        }

        if (is_array($result) || $collectionClass === ClassName::ILLUMINATE_DATABASE_ELOQUENT_COLLECTION)
        {
            foreach ($result as &$rs)
            {
                $rs->modifyBitToBoolean($bitColumns);
            }
        }
        else
        {
            $result->modifyBitToBoolean($bitColumns);
        }

        return $result;
    }

    /**
     * Get the first item in the collection.
     *
     * @param Collection|null $result
     * @return Model|null|mixed
     */
    public static function getFirstResult($result)
    {
        if (!$result)
        {
            return null;
        }

        return $result->first();
    }

    public static function getBySQL(string $sql, array $bindings = [], array $bitColumns = [], string $connection = null)
    {
        $result = $connection ? DB::connection($connection)->select(DB::raw($sql), $bindings) : DB::select(DB::raw($sql), $bindings);
        $result = self::hydrate($result);

        return Model::modifyBitToBooleanInResult($result, $bitColumns);
    }

    public static function updateBySQL(string $sql, array $bindings = [])
    {
        DB::update(DB::raw($sql), $bindings);
    }

    /**
     * Modify data retrieved from the database to boolean since the database is returning int for BIT/BOOLEAN data type.
     * For query involving more than one tables or columns with aliases can be passed in through $bitColumns.
     *
     * @param array $bitColumns the column name or aliases to be modified to BOOLEAN.
     */
    protected function modifyBitToBoolean(array $bitColumns = [])
    {
        $booleanColumns = self::getBooleanColumns();

        foreach ($booleanColumns as $column)
        {
            if(!isset($this->$column))
            {
                continue;
            }

            $this->$column = (bool) $this->$column;
        }

        foreach ($bitColumns as $column)
        {
            if(!isset($this->$column))
            {
                continue;
            }

            $this->$column = (bool) $this->$column;
        }
    }

    private static function getBooleanColumns()
    {
        if (self::$booleanColumns)
        {
            return self::$booleanColumns;
        }

        self::$booleanColumns = BooleanColumns::getConstants();

        return self::$booleanColumns;
    }
}
