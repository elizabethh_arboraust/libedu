<?php

namespace App\Helpers;

use App\Api\ErrorCode;
use App\Enums\PHP\ClassName;
use App\Exceptions\CustomException;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class ExceptionHelper
{
    /**
     * Get error object regardless of the type of Exception thrown.
     *
     * @param Exception $exception
     * @return Response
     */
    public static function getErrorObject(Exception $exception)
    {
        if (get_class($exception) !== ClassName::APP_EXCEPTIONS_CUSTOM_EXCEPTION)
        {
            $code = $exception->getCode();
            $code = is_int($code) ? $code : ErrorCode::NON_INT_CODE;

            $exception = new CustomException($exception->getMessage(), $code, $exception);
        }

        return $exception->getErrorObject();
    }
}
