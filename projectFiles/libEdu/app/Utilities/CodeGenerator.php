<?php

namespace App\Utilities;

class CodeGenerator
{
    /**
     * Generate random code.
     *
     * @param int $from_base used in base conversion method, see https://www.php.net/manual/en/function.base-convert.php for more information
     * @param int $to_base used in base conversion method, see https://www.php.net/manual/en/function.base-convert.php for more information
     * @param int $limit the total length of the code desired.
     * @return string
     */
    public static function generateRandomCode(int $from_base, int $to_base, int $limit)
    {
        $randomNumber = mt_rand();
        $uniqueID = uniqid($randomNumber);
        $sha1 = sha1($uniqueID);
        $baseConvert = base_convert($sha1, $from_base, $to_base);

        return substr($baseConvert, 0, $limit);
    }
}
