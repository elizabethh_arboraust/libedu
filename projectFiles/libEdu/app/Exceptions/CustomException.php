<?php
/**
 * *
 * * Created by Chris Graham
 * * Define a custom exception class
 * * Copyright (c) 2019. - Arbor Australia Pty Ltd
 *
 */

namespace App\Exceptions;

use App\Enums\HTTP\HTTPResponseCode;
use App\Logging\ErrorLogging;
use App\Entities\ErrorCodeGenerator;
use App\Traits\ModifiesHTTPResponseCode;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class CustomException extends Exception
{
    use ModifiesHTTPResponseCode;
    
    protected $errorReferenceNumber;
    protected $isShownOnProduction;
    protected $statusCode;

    /**
     * This can be used if no specific things need to be done in catch block.
     *
     * @param $message
     * @param $code
     * @param null $exception
     * @param false $isRender false by default to return a JSON error, true to render the error to screen and stop script execution. (only during debug mode (debug = true))
     * @return false|string
     */
    public static function handleError($message, $code, $exception = null, $isRender = false)
    {
        new self($message, $code, $exception);

        $error = (object) array (
            'result' => null,
            'error' => $message
        );
        $error = json_encode($error);

        $debug = config('app.debug');
        if ($debug == 1){
            if ($isRender)
            {
                print_r($error);
                die();
            }
            else
            {
                header('content-type: text/javascript');

                return $error;
            }
        } else {
            header('HTTP/1.0 403 Forbidden');
            http_response_code(HTTPResponseCode::FORBIDDEN);
            die();
        }
    }

    /**
     * Generate error reference number. This is a 6 digits alpha numeric code.
     * This code can be referenced by the customer to help us finding the exact error in the log.
     *
     * @return string
     */
    private static function generateErrorReferenceNumber()
    {
        return ErrorCodeGenerator::generate();
    }

    /**
     * Redefine the exception so message isn't optional
     *
     * @param string $message
     * @param int $code
     * @param Exception|null $previous
     * @param bool $isShownOnProduction false by default to not show the error message in production, true to show.
     * @param int $statusCode HTTP Status Code 400 by default
     */
    public function __construct(string $message, int $code, Exception $previous = null, $isShownOnProduction = false, int $statusCode = HTTPResponseCode::BAD_REQUEST)
    {
        // make sure everything is assigned properly
        parent::__construct($message, $code, $previous);
        $this->errorReferenceNumber = self::generateErrorReferenceNumber();
        $this->isShownOnProduction = $isShownOnProduction;
        $this->statusCode = $statusCode;

        $this->logError($message);
    }

    /**
     * Get the error object in JSON.
     *   - Development: 400 for all errors and show the error message
     *   - Production: 403 for most errors without any error message, 400 for errors with error message if we need to notify the tablet
     *
     * @return Response
     */
    public function getErrorObject()
    {
        $error['error'] =
            [
                'message' => $this->message,
            ];

        $httpResponseCode = HTTPResponseCode::FORBIDDEN;
        $content = '';

        if ($this->isShownOnProduction || config('app.debug'))
        {
            $httpResponseCode = $this->statusCode === HTTPResponseCode::METHOD_NOT_ALLOWED ? $this->statusCode : HTTPResponseCode::BAD_REQUEST;
            $content = $this->message ? json_encode($error) : null;
        }

        http_response_code($httpResponseCode);

        return new Response($content, http_response_code());
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    public function getErrorAsArray()
    {
        $error = array
        (
            'code'=>$this->code,
            'message'=>$this->message,
        );

        if ($this->errorReferenceNumber)
        {
            $error += ['Error Reference Number' => '#' . $this->errorReferenceNumber];
        }

        return $error;
    }

    /**
     * Display error on screen if app is in debug mode.
     */
    public function displayError()
    {
        $error = (object) array (
            'error' => null,
        );

        $error->error = config('app.debug') ? $this->getMessage() : true;

        print_r(json_encode($error));
        die();
    }

    private function getErrorMessageAndStackTraces($message)
    {
        return $message . "\n" .
            ($this->errorReferenceNumber ? "Error Reference Number: #" . $this->errorReferenceNumber . "\n" : '') .
            $this->getTraceAsString() . "\n";
    }

    private function logError($message)
    {
        (new ErrorLogging)->log($this->getErrorMessageAndStackTraces($message));
    }
}
