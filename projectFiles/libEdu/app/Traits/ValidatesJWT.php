<?php

namespace App\Traits;

use App\Api\ErrorCode;
use App\Enums\Messages\Errors\JWTErrorMessage;
use App\Enums\HTTP\HTTPAuthenticationType;
use App\Enums\HTTP\HTTPHeader;
use App\Exceptions\CustomException;
use App\Helpers\ExceptionHelper;
use Exception;

/**
 * Validate JWT from HTTP Authorization header.
 *
 * @package App\Traits
 */
trait ValidatesJWT
{
    private function getJWTFromAuthorizationHeader()
    {
        try
        {
            $isBearerFound = false;
            $isJWTFound = false;
            $jwt = null;

            $authorizationHeader = $this->getAuthorizationHeader();

            if (!$authorizationHeader)
            {
                return false;
            }

            foreach ($authorizationHeader as $auth)
            {
                // Ignore empty slots
                if (trim($auth) === '')
                {
                    continue;
                }

                if (!$isBearerFound)
                {
                    if (strtolower($auth) === strtolower(HTTPAuthenticationType::BEARER))
                    {
                        $isBearerFound = true;
                        continue;
                    }
                }

                if ($isBearerFound && !$isJWTFound)
                {
                    $jwt = $auth;
                    break;
                }
            }

            if (!$jwt)
            {
                throw new CustomException (JWTErrorMessage::JWT_NOT_FOUND, ErrorCode::JWT_NOT_FOUND);
            }

            return $jwt;
        }
        catch(Exception $exception)
        {
            return ExceptionHelper::getErrorObject($exception);
        }
    }

    private function getAuthorizationHeader()
    {
        try
        {
            $headers = getallheaders();

            if (!array_key_exists(HTTPHeader::AUTHORIZATION, $headers))
            {
                throw new CustomException(JWTErrorMessage::NO_AUTHORIZATION_HEADER, ErrorCode::NO_AUTHORIZATION_HEADER);
            }

            return explode(' ', $headers[HTTPHeader::AUTHORIZATION]);
        }
        catch (CustomException | Exception $exception)
        {
            return ExceptionHelper::getErrorObject($exception);
        }
    }
}
