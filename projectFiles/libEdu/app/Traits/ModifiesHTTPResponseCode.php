<?php

namespace App\Traits;

use App\Enums\HTTP\HTTPResponseCode;

/**
 * Modify HTTP Response Code of the page.
 */
trait ModifiesHTTPResponseCode
{
    /**
     * Use this for most errors in production where we don't want to leave any info for the user (to prevent attack).
     */
    public static function error()
    {
        http_response_code(HTTPResponseCode::FORBIDDEN);
    }

    /**
     * Use this for errors in development or some errors in production where we have to notify the user of the errors to avoid confusion.
     */
    public static function errorReturnMessage()
    {
        http_response_code(HTTPResponseCode::BAD_REQUEST);
    }

    /**
     * Use this when a request is successful.
     */
    public static function success()
    {
        http_response_code(HTTPResponseCode::OK);
    }

    /**
     * Use this when a request is successful but without any result.
     */
    public static function successNoResult()
    {
        http_response_code(HTTPResponseCode::NO_CONTENT);
    }

    /**
     * Use this when a request is successful.
     * This will modify HTTP Response Code to 200 when there are any result or 204 when there are no result.
     *
     * @param null $result
     */
    public static function successByResult($result = null)
    {
        if (!$result)
        {
            ModifiesHTTPResponseCode::successNoResult();
        }

        ModifiesHTTPResponseCode::success();
    }
}
