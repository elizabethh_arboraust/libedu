<?php

namespace App\Entities;

use App\Utilities\CodeGenerator;

class ErrorCodeGenerator
{
    // Total digits
    private const LENGTH = 6;

    // For number base conversion
    private const FROM_BASE = 16;
    private const TO_BASE = 36;

    /**
     * Generate error code. This code can be shown and referenced to the customer to help us find the exact error in the log.
     *
     * @return string
     */
    public static function generate()
    {
        return strtoupper(CodeGenerator::generateRandomCode(self::FROM_BASE, self::TO_BASE, self::LENGTH));
    }
}
