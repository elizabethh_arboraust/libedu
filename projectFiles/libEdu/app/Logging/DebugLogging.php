<?php

namespace App\Logging;

use App\Enums\PHP\PHPDataType;
use Illuminate\Support\Facades\Log;
use stdClass;

class DebugLogging
{
    private static $logger;

    /**
     * @param string|stdClass|array $message
     */
    public static function log($message)
    {
        $logger = !self::$logger ? new self : self::$logger;

        Log::debug($logger->formatLog($message));
    }

    public static function print($message = '')
    {
        echo "\r\n";
        echo "<br /><br />";
        print_r($message);
        echo "\r\n";
        echo "<br /><br />";
    }

    /**
     * Format object in an easy to read format in the log.
     *
     * @param string|stdClass|array $message
     * @return string
     */
    private function formatLog($message)
    {
        switch (gettype($message))
        {
            case PHPDataType::OBJECT:
            case PHPDataType::STD_CLASS:
                $message = (array) $message;
                break;
        }

        return print_r($message, true);
    }
}
