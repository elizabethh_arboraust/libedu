<?php

namespace App\Logging;

use Illuminate\Support\Facades\Log;

class ErrorLogging
{
    public function log(string $message)
    {
        Log::error($this->formatLog($message));
    }

    protected function formatLog($message)
    {
        return $message;
    }
}
