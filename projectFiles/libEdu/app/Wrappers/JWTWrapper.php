<?php

namespace App\Wrappers;

use App\Api\ErrorCode;
use App\Enums\JWT\JWTAlgorithm;
use App\Enums\Messages\Errors\JWTErrorMessage;
use App\Enums\JWT\JWTClaim;
use App\Exceptions\CustomException;
use App\Models\User;
use Exception;
use Firebase\JWT\JWT;

class JWTWrapper
{
    public static function generate(User $user)
    {
        $token = self::getToken();
        $token['username'] = $user->userName;
        $token['password'] = decrypt($user->userPasswordEncrypted);

        return JWT::encode($token, config('app.moodleJWTSecret'));
    }

    /**
     * Decode JWT.
     * The secret is from BROADCAST_JWT_SECRET in .env file.
     *
     * @param string $jwt
     * @return object|false the decoded object if JWT authentication succeed or false if unsuccessful.
     */
    public static function decode(string $jwt)
    {
        try
        {
            $decoded = JWT::decode($jwt, config('app.broadcastJWTSecret'), [JWTAlgorithm::HS256]);
            self::validateJWT($decoded);

            return $decoded;
        }
        catch(Exception $e)
        {
            return false;
        }
    }

    private static function getToken()
    {
        return
            [
                JWTClaim::ISSUER => config('jwt.iss'),
                JWTClaim::AUDIENCE => config('jwt.aud'),
                JWTClaim::ISSUED_AT_TIME => config('jwt.iat'),
                JWTClaim::EXPIRATION_TIME => config('jwt.exp'),
                JWTClaim::NOT_BEFORE => config('jwt.nbf'),
            ];
    }

    public static function validateJWT($decoded)
    {
        $iatKey = JWTClaim::ISSUED_AT_TIME;
        $expKey = JWTClaim::EXPIRATION_TIME;

        if (!array_key_exists($iatKey, $decoded))
        {
            throw new CustomException(JWTErrorMessage::IAT_CLAIM_IS_REQUIRED, ErrorCode::JWT_IAT_CLAIM_IS_REQUIRED);
        }

        $nbf = config('jwt.nbf');

        if ($decoded->$iatKey < $nbf)
        {
            throw new CustomException(JWTErrorMessage::MUST_BE_AFTER . ' ' . date(config('date.datetime_format'), $nbf), ErrorCode::JWT_MUST_BE_AFTER);
        }

        if ((array_key_exists($expKey, $decoded) && $decoded->$expKey < time()) || (!array_key_exists($expKey, $decoded) && $decoded->$iatKey + config('jwt.exp_second') < time()))
        {
            throw new CustomException(JWTErrorMessage::EXPIRED, ErrorCode::JWT_EXPIRED);
        }
    }
}
