<?php

namespace App\Http\Middleware;

use App\LocalUser;
use App\LoginUser;
use App\User;
use App\UserRole;
use Closure;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Session;

class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        error_log(json_encode($_GET));

        try {
            $jwt = getallheaders()['authorization'];
            $privateKey = config('jwt.ssoKey');

            if ($jwt) {
                $decoded = JWT::decode($jwt, $privateKey, ['HS256']);

                error_log($decoded->userName);
                $user = LoginUser::where('userName', $decoded->userName )->first();

                if ($user) {
                    Session::put('jwt', true);
                    Session::put('user', [
                        'name' => $user->userName,
                        'group' => $user->userGroup,
                    ]);

                 } else {
                    error_log("User Not Valid");
                    throw new \Exception();
                }
            } else {
                error_log("JWT Not Valid");
                throw new \Exception();
            }
        } catch (\Exception $e) {
            error_log($e->getMessage());
            if (!session()->has('user')) {
                return redirect('login');
            }
        }

        return $next($request);
    }
}
