<?php

namespace App\Http\Controllers;

use App\Api\ErrorCode;
use App\Enums\Messages\Errors\UserErrorMessage;
use App\Exceptions\CustomException;
use App\Http\Controllers\Core\JWTController;
use App\Models\Broadcast;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class OnboardingController extends JWTController
{
    /**
     * Onboard/offboard depending on 'ActiveStatus'
     *
     * @param Request $request
     * @return mixed|Response
     */
    public function onboard(Request $request)
    {
        $function = function($request)
        {
            $post = $request->post();
            $id = Broadcast::saveData($post);

            DB::beginTransaction();
            User::onboard($post);
            Broadcast::deleteData($id);
            DB::commit();

            User::onboardToMoodle($post);
        };

        return $this->getResult($function, [$request]);
    }
}