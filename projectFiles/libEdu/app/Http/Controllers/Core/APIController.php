<?php

namespace App\Http\Controllers\Core;

use App\Helpers\ExceptionHelper;
use App\Helpers\ResponseHelper;
use Closure;
use Exception;
use Symfony\Component\HttpFoundation\Response;

/**
 * This class contains getResult() that returns a HTTP Response object containing HTTP Response Code and output to the body.
 */
class APIController extends JWTController
{
    /**
     * This function is intended for a web service call where the app secret is validated and enclosed in try-catch block.
     *
     * @param Closure $function the function to be called to get the result.
     * @param array|null $appTypes null when calling this function from within php code where app validation is not needed, more than one app can be passed in if a web service call is available for more than one apps.
     * @param array $args
     * @return Response
     */
    protected function getResult(Closure $function, $appTypes = null, array $args = [])
    {
        $result = parent::getResult($function, $appTypes, $args);

        if (ResponseHelper::isResponse($result))
        {
            return $result;
        }

        return $this->getResponse($result);
    }

    /**
     * Get the response object which will print result and set the correct HTTP Response Code.
     *
     * @param $content
     * @return Response
     */
    private function getResponse($content = '')
    {
        try
        {
            $content = $content ? json_encode($content) : '';

            return new Response($content, http_response_code());
        }
        catch (Exception $exception)
        {
            return ExceptionHelper::getErrorObject($exception);
        }
    }

}
