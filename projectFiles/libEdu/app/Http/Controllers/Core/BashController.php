<?php

namespace App\Http\Controllers\Core;

use App\Entities\BashResponse;
use App\Helpers\ResponseHelper;
use App\Logging\DebugLogging;
use Closure;

/**
 * This class contains getResult() that returns a JSON to bash scripts.
 */
class BashController extends JWTController
{
    protected function getResult(Closure $function, $appTypes = null, array $args = [])
    {
        $result = parent::getResult($function, $appTypes, $args);

        // if there's any error thrown, $result will be a Response object
        if (ResponseHelper::isResponse($result))
        {
            return (new BashResponse(null, $result))->jsonSerialize();
        }

        return (new BashResponse($result))->jsonSerialize();
    }
}
