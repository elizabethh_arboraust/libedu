<?php

namespace App\Http\Controllers\Core;

use App\Helpers\ExceptionHelper;
use Closure;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class WebController extends Controller
{
    /**
     * This function is intended for a web service call enclosed in try-catch block.
     *
     * @param Closure $function the function to be called to get the result.
     * @param array $args
     * @return mixed|Response
     */
    protected function getResult(Closure $function, array $args = [])
    {
        try
        {
            return call_user_func_array($function, $args);
        }
        catch (Exception $exception)
        {
            return ExceptionHelper::getErrorObject($exception);
        }
    }
}
