<?php

namespace App\Http\Controllers\Core;

use App\Api\ErrorCode;
use App\Enums\Messages\Errors\JWTErrorMessage;
use App\Exceptions\CustomException;
use App\Helpers\ExceptionHelper;
use App\Logging\DebugLogging;
use App\Traits\ModifiesHTTPResponseCode;
use App\Traits\ValidatesJWT;
use App\Wrappers\JWTWrapper;
use Closure;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class JWTController extends Controller
{
    use ModifiesHTTPResponseCode, ValidatesJWT;

    protected function errorAuthenticatingJWT()
    {
        throw new CustomException(JWTErrorMessage::JWT_AUTH_FAIL, ErrorCode::JWT_AUTH_FAIL);
    }

    /**
     * This function is intended for a web service call where the app secret is validated and enclosed in try-catch block.
     *
     * @param Closure $function the function to be called to get the result.
     * @param array $args
     * @return mixed|Response
     */
    protected function getResult(Closure $function, array $args = [])
    {
        try
        {
            if(!$this->isAuthenticated())
            {
                $this->errorAuthenticatingJWT();
            }

            return call_user_func_array($function, $args);
        }
        catch (Exception $exception)
        {
            return ExceptionHelper::getErrorObject($exception);
        }
    }

    private function isAuthenticated()
    {
        $jwt = $this->getJWTFromAuthorizationHeader();

        if (!$jwt)
        {
            return false;
        }

        $decoded = JWTWrapper::decode($jwt);

        if (!$decoded)
        {
            return false;
        }

        return true;
    }
}
