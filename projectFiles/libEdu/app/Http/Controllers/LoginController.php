<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Core\Controller;
use App\LoginUser;
use App\User;
use App\Api\ErrorMessage;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cookie;

/**
 * Class LoginController
 * @package App\Http\Controllers
 */
class LoginController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $link = url()->previous();
        if ($link !== url()->current()) {
            Session::put('intended', $link);
        }
        return view('login');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function login(Request $request)
    {
        $link = session('intended');
        if (!$link) {
            $link = "main";
        }

        $response = [
            'result' => false,
            'link' => $link,
        ];

        $username = $request['userName'];
        $password = $request['userPassword'];

        $user = LoginUser::where('userName', $username)->first();

        if ($user) {
            if (Hash::check($password, $user->userPassword)) {
                Session::put('user', [
                    'name' => $user->userName,
                    'group' => $user->userGroup,
                ]);
                $response['result'] = true;
            } else {
                $response['error']['message'] = ErrorMessage::INVALID_USERNAME_PASSWORD;
            }
        } else {
            $response['error']['message'] = ErrorMessage::INVALID_USERNAME_PASSWORD;
        }

        Session::forget('intended');
        Session::save();
        return $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout(Request $request)
    {
        Session::flush();
        return redirect('main');
    }
}
