<?php

namespace App\Http\Controllers;

use App\Api\ErrorCode;
use App\Enums\JWT\JWTAlgorithm;
use App\Enums\Messages\Errors\JWTErrorMessage;
use App\Enums\Messages\Errors\UserErrorMessage;
use App\Exceptions\CustomException;
use App\Http\Controllers\Core\WebController;
use App\Models\User;
use App\Wrappers\JWTWrapper;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;

class SSOController extends WebController
{
    public function sso(Request $request)
    {
        $function = function($request)
        {
            //$jwt = getallheaders()["authorization"];
            $jwt = $_SERVER['HTTP_AUTHORIZATION'];
            $decoded = JWT::decode($jwt, config('app.longbowJWTSecret'), [JWTAlgorithm::HS256]);
            JWTWrapper::validateJWT($decoded);

            if (!$decoded)
            {
                throw new CustomException(JWTErrorMessage::JWT_AUTH_FAIL, ErrorCode::JWT_AUTH_FAIL);
            }

            if (!property_exists($decoded, 'userName'))
            {
                throw new CustomException(JWTErrorMessage::USER_NAME_NOT_FOUND, ErrorCode::DATA_NOT_FOUND);
            }

            $userName = $decoded->userName;
            $user = User::getUser($userName);

            if (!$user)
            {
                throw new CustomException(UserErrorMessage::USER_NOT_FOUND, ErrorCode::DATA_NOT_FOUND);
            }

            $jwt = JWTWrapper::generate($user);

            return redirect(config('app.moodleServer') . '/login/index.php?jwt=' . $jwt);
        };

        return $this->getResult($function, [$request]);
    }
}