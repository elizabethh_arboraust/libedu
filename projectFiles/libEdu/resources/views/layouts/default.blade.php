<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('includes.head')
    </head>
    <body>
{{--        @if(app()->environment('production'))--}}
{{--            @include('includes.nav')--}}
{{--        @endif--}}

        <div class="row" id="body-row">
            @include('includes.sidebar')

            <div class="col p-4">
                @yield('content')
            </div>
        </div>

        @if(app()->environment('production'))
            @include('includes.footer')
        @endif
    </body>
</html>
