<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('includes.head')
    </head>
    <body class="body">
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>
