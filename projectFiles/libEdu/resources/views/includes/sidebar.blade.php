<div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
    <!-- d-* hiddens the Sidebar in smaller devices. Its itens can be kept on the Navbar 'Menu' -->
    <!-- Bootstrap List Group -->
    <ul class="list-group">
        <a href="#top" data-toggle="sidebar-colapse" class="bg-dark list-group-item list-group-item-action d-flex align-items-center">
            <div class="d-flex w-100 justify-content-start align-items-center">
                <span id="collapse-icon" class="fa fa-2x mr-3"></span>
                <span id="collapse-text" class="menu-collapsed">Collapse</span>
            </div>
        </a>

        @if(!session('user'))
            <a class="login-user btn btn-sm mt-2" href="login">
                <i class="fa fa-sign-in" aria-hidden="true"></i> Login
            </a>
        @else
            <small class="login-user text-light mx-3 my-4">
                Welcome, <strong>{{ session('user.name') }}</strong>!
            </small>
        @endif


        <!-- Submenu content -->
            @foreach(cache('widgets') as $groups)
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small>{{$groups['groupData']->name}}</small>
            </li>

                @foreach($groups['widgets'] as $widget)
                    <a href="{{ $widget->link }}" class="bg-dark list-group-item list-group-item-action">
                        <div class="d-flex w-100 justify-content-start align-items-center">

                            <span class="{{ $widget->icon }}"></span>
                            <span class="menu-collapsed" href="{{ $widget->link }}">{{ $widget->name }}</span>

                        </div>
                    </a>
                @endforeach

            @endforeach

        <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
            <small>Dev</small>
        </li>

        <a href="api_test" class="bg-dark list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-start align-items-center">
                <span class="fa fa-question fa-fw mr-3"></span>
                <span class="menu-collapsed">Api Tester</span>
            </div>
        </a>

    </ul><!-- List Group END-->
    <br>

</div><!-- sidebar-container END -->

<script>
    // Hide submenus
    $('#body-row .collapse').collapse('hide');

    // Collapse/Expand icon
    $('#collapse-icon').addClass('fa-angle-double-left');

    // Collapse click
    $('[data-toggle=sidebar-colapse]').click(function() {
        SidebarCollapse();
    });

    function SidebarCollapse () {
        $('.login-user').toggleClass('d-none');
        $('.menu-collapsed').toggleClass('d-none');
        $('.sidebar-submenu').toggleClass('d-none');
        $('.submenu-icon').toggleClass('d-none');
        $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');

        // Treating d-flex/d-none on separators with title
        var SeparatorTitle = $('.sidebar-separator-title');
        if ( SeparatorTitle.hasClass('d-flex') ) {
            SeparatorTitle.removeClass('d-flex');
        } else {
            SeparatorTitle.addClass('d-flex');
        }

        // Collapse/Expand icon
        $('#collapse-icon').toggleClass('fa-angle-double-left fa-angle-double-right');
    }
</script>

