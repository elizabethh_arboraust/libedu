<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="/lib/bootstrap/css/bootstrap.css">
<!--link rel="stylesheet" type="text/css" href="/public/lib/fontawesome/css/all.min.css">
<link rel="stylesheet" type="text/css" href="/public/css/jquery.skedTape.css">
<link rel="stylesheet" type="text/css" href="/public/css/bootstrap4-toggle.min.css"-->
<link rel="stylesheet" type="text/css" href="{{ Asset::get('css/common.css') }}">

<!-- jQuery first, Popper, then Bootstrap JS -->
<script type="text/javascript" src="/lib/jquery/jquery.min.js"></script>
<!--script type="text/javascript" src="/public/js/popper.min.js"></script-->
<script type="text/javascript" src="/lib/bootstrap/js/bootstrap.js"></script>
<!--script type="text/javascript" src="/public/lib/fontawesome/js/all.min.js"></script>
<script type="text/javascript" src="/public/js/moment.min.js"></script>
<script type="text/javascript" src="/public/js/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="/public/js/jquery.skedTape.js"></script>
<script type="text/javascript" src="/public/js/bootstrap4-toggle.min.js"></script-->
<script type="text/javascript" src="{{ Asset::get('js/common.js') }}"></script>

@yield('head')

<!-- Back to top button -->
<span id="back-to-top"></span>
