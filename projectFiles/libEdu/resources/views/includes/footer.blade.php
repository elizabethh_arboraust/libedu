<!-- Footer -->
<footer class="page-footer font-small stylish-color-dark pt-4">
    <!-- Spinner -->
    <div id="overlay">
        <div class="cs-loader">
            <div class="cs-loader-inner">
                <label>	●</label>
                <label>	●</label>
                <label>	●</label>
                <label>	●</label>
                <label>	●</label>
                <label>	●</label>
            </div>
        </div>
    </div>
    <!-- Spinner -->

    <!-- Copyright -->
    <div class="footer-copyright text-center font-weight-light py-4">
        © 1993-2019 <a href="http://www.arboraust.com.au/" target="_blank"> ARBOR Technology Corp.</a>
    </div>
    <!-- Copyright -->
</footer>
<!-- Footer -->
