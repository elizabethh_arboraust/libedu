@extends('layouts.content')
@section('content')
    <div class="content-center">
        <div class="card p-2 py-2 border-0 rounded" style="width: 20rem;">
            <div class="card-body wrapper-login">
                <form id="loginForm" class="needs-validation" novalidate>
                    <div class="form-group">
                        <label>USERNAME</label>
                        <input type="text" class="form-control form-control-lg" id="userName" required>
                    </div>
                    <div class="form-group">
                        <label>PASSWORD</label>
                        <input type="password" class="form-control form-control-lg" id="userPassword" required>
                        <span class="btn-show-pass" onclick="togglePasswordVisibility(this)">
                            <i class="fa fa-eye text-secondary"></i>
                        </span>
                    </div>
                    <button type="submit" class="btn btn-lg btn-block btn-dark">Login</button>
                </form>
            </div>
        </div>
    </div>

    <script>
        function login() {
            const userName = $("#userName").val();
            const userPassword = $("#userPassword").val();

            if ($('#loginForm')[0].checkValidity() === true) {
                $.ajax({
                    type: 'POST',
                    url: 'login',
                    data: {userName, userPassword},
                    beforeSend: function () {
                        loading(true);
                    },
                    success: function (data) {
                        if (data.result === true) {
                            window.location.href = data.link;
                        } else if (data.error) {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });

                            Toast.fire({
                                type: 'error',
                                title: 'Failed',
                                text: data.error.message
                            })
                        }
                    },
                    complete: function () {
                        loading(false);
                    }
                });
            }
        };

        function togglePasswordVisibility(elem) {
            const $input = $(elem).prev();
            if ($input.attr("type") === "password") {
                $input.attr("type", "text");
            } else {
                $input.attr("type", "password");
            }

            $(elem).find('i').toggleClass('fa-eye fa-eye-slash');
        }

        $(function() {
            $('#loginForm').submit(function(event) {
                event.preventDefault();
                login();
            });
        })
    </script>
@endsection
