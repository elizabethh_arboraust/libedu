$(function () {
    'use strict';

    /* Global Ajax Event Handlers */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $( document ).ajaxStart(function() {
        loading(true);
    });
    $( document ).ajaxComplete(function() {
        loading(false);
    });
    $( document ).ajaxError(function() {
        Swal.fire({
            type: 'error',
            title: 'Error!',
            text: 'An AJAX HTTP error occurred'
        });
    });

    /* Form Validation */
    window.addEventListener('load', function() {
        const forms = document.getElementsByClassName('needs-validation');
        const validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);

    /* Back To Top Button */
    $( window ).on('scroll', function () {
        toggleBackToTopButton()
    });
    $('#back-to-top').on('click', scrollBackToTop);

    /* Clear Input */
    $('.input-clear').click(function () {
        $(this).prev().val('').focus()
    });

    /* Nav Bar */
    // activateNavbar();
});

function loading(status) {
    if (status) {
        $("#overlay").fadeIn(300);
    } else {
        $("#overlay").fadeOut(300);
    }
}

function scrollDown(elem) {
    const offset = $(elem).offset();
    if (offset !== undefined) {
        $('html, body').animate({
            scrollTop: offset.top
        },300);
    }
}

function toggleBackToTopButton() {
    if ($(window).scrollTop() > 300) {
        $("#back-to-top").addClass('show');
    } else {
        $("#back-to-top").removeClass('show');
    }
};

function scrollBackToTop() {
    $('html, body').animate({
        scrollTop: 0
    }, 300);
};

function activateNavbar() {
    $(".navbar a").each(function () {
        if (window.location.href === this.href) {
            $(this).closest("li").addClass("active");
            $(this).parents(".dropdown").addClass("active");
        }
    });
};
