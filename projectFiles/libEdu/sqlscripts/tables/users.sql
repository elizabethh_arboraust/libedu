CREATE TABLE IF NOT EXISTS `users` (
  `userID` INT NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(100) NOT NULL,
  `middleName` VARCHAR(100) NULL,
  `lastName` VARCHAR(100) NOT NULL,
  `userName` VARCHAR(30) NOT NULL,
  `userPassword` VARCHAR(100) NULL,
  `userPasswordEncrypted` VARCHAR(256) NULL,
  `userRFID` TEXT NULL,
  `userFPrint` TEXT NULL,
  `birthDate` DATE NULL,
  `gender` VARCHAR(10) NOT NULL,
  `personType` VARCHAR(20) NOT NULL,
  `aliasesJSON` TEXT NULL,
  `isActive` BIT(1) NOT NULL DEFAULT FALSE,
  `lastOIMSLocationUpdateAt` TIMESTAMP NULL,
  `lastOnBoardingAt` TIMESTAMP NULL,
  `agencyLocation` VARCHAR(100) NULL,
  `agencyLocationID` VARCHAR(100) NULL,
  `housingLocation` VARCHAR(100) NULL,
  `housingLocationID` VARCHAR(100) NULL,
  `level1Code` VARCHAR(100) NULL,
  `level2Code` VARCHAR(100) NULL,
  `level3Code` VARCHAR(100) NULL,
  `level4Code` VARCHAR(100) NULL,
  `createdBy_UserID` INT NULL,
  `updatedBy_UserID` INT NULL,
  `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT PK_users_ID PRIMARY KEY (`userID`),
  CONSTRAINT UQ_users_userName UNIQUE KEY (`userName`)
);