CREATE TABLE IF NOT EXISTS broadcasts
(
    ID INT auto_increment,
    userName VARCHAR(50) null,
    data TEXT null,
    dateTime timestamp default CURRENT_TIMESTAMP null,
    CONSTRAINT PK_broadcasts_ID PRIMARY KEY (`ID`)
);