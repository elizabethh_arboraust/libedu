#!/bin/bash
#################################################################################
# This script will do these on CREATE:
# 1. Get all files from git
# 2. Generate new app key in .env file
# 3. Zip the project
#
# This script will do these on UPDATE:
# 1. Get patches from git
# 2. Zip the project
#################################################################################

echo '
  Usage: bash zipProject.sh --dest-env=test [--mode=create]  [--prev-tag=previousTagName] [--prev-branch=previousBranchName]
  --mode                    create or update (update by default, so if this is omitted this will do update)
  --prev-tag                if updating from a previous tag. Cannot be used together with --prev-branch
  --prev-branch             if updating from a previous branch. Cannot be used together with --prev-tag
  --dest-env                dev, test or prod. This will copy the appropriate env file to .env (e.g. --dest-env=test will copy .env.test to .env)
  '

project=libEdu
zipFile="${project}Deploy.zip"
projectFilesDir=".."
bashDir="$projectFilesDir/bash"
includeDir="$bashDir/include"
appDir="$projectFilesDir/$project"
destDir="../"

zipFromRoot="../../deployment/$zipFile"
if [[ $* == *"--test"* ]] && [[ -f "$zipFromRoot" ]]
then
  rm "$zipFromRoot"
fi

branchTagHelp='If updating by branch name, use --prev-branch
If updating by tag name, use --prev-tag'

chooseBranchTagHelp="Select to update either by branch or tag
$branchTagHelp"

source "$includeDir/include.sh"

mode=${args[mode]}
validateMode $mode

destEnv=${args[dest-env]}
validateDestEnv $destEnv

if [ "$mode" != "$CREATE" ]
then
  if [ -z ${args[prev-tag]} ] && [ -z ${args[prev-branch]} ]
  then
    echo "$branchTagHelp"
    exit
  elif [ ! -z ${args[prev-tag]} ] && [ ! -z ${args[prev-branch]} ]
  then
    echo "$chooseBranchTagHelp"
    exit
  fi

  # using tag. Checking tag exist
  if [ ! -z ${args[prev-tag]} ]
  then
    if ! GIT_DIR="../../.git" git rev-parse "${args[prev-tag]}^{tag}" >/dev/null 2>&1
    then
        echo "prev-tag '${args[prev-tag]}' not found"
        exit
    fi
    prev=${args[prev-tag]}
  fi

  # using branch. Checking branch exist
  if [ ! -z ${args[branch]} ]
  then
    if ! GIT_DIR="../../.git" git rev-parse --verify "${args[prev-branch]}" >/dev/null 2>&1
    then
        echo "prev-branch '${args[prev-branch]}' not found"
        exit
    fi
    prev=${args[prev-branch]}
  fi
fi

# projectFiles
cd ..

# onboarding_git
cd ..

branch=$(git rev-parse --abbrev-ref HEAD)
git checkout $branch || exit

# zip differences between HEAD and previous branch/tag
zip="deployment/$zipFile"
git archive -o $zip HEAD $(git diff -u --name-only -z --diff-filter=ACMRT $prev..HEAD | xargs -0)

cd deployment

# remove deployment folder and its content from the zip
zip -d "$zipFile" "deployment/" "deployment/*"

# add deployProject.sh to zip root
zip "$zipFile" "deployProject.sh"
zip "$zipFile" "$project.conf"

# libEdu_git
cd ..

zip -ru "deployment/$zipFile" "projectFiles/$project/vendor/"

cd "projectFiles/$project"
# backup current .env file and put the requested one to .env
cp ".env" ".env.backup"
cp ".env.$destEnv" ".env"

#if [ "$mode" = "$CREATE" ]
#then
#  php artisan key:generate || exit
#fi

# project files
cd ..

# onboarding git
cd ..

# add new .env to the zip
zip -ru "$zip" "projectFiles/$project/.env"

cd "projectFiles/$project"

# restore .env file in this folder
cp ".env.backup" ".env"
rm -f ".env.backup"