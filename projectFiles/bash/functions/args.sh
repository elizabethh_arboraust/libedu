#!/bin/bash
######################################################################################################
# Get the arguments passed in
# Using variable $args as a global variable so we can get associative array out from the function.
######################################################################################################
declare -A args
function getArguments()
{
  for ARGUMENT in "$@"
  do
      KEY=$(echo $ARGUMENT | cut -f1 -d=)
      VALUE=$(echo $ARGUMENT | cut -f2 -d=)

      case "$KEY" in
              --mode)         args[mode]=${VALUE,,} ;;
              --tag)          args[tag]=${VALUE} ;;
              --prev-tag)     args[prev-tag]=${VALUE} ;;
              --branch)       args[branch]=${VALUE} ;;
              --prev-branch)       args[prev-branch]=${VALUE} ;;
              --dest-env)       args[dest-env]=${VALUE,,} ;;
              *)
      esac
  done
}
getArguments "$@"

######################################################################################################
# Get mode 'create' (for first run/install) or 'update'
# This function compares the mode in a case-insensitive way.
######################################################################################################

function validateMode()
{
  mode=${1,,}
  [ "$mode" != $CREATE ] && [ "$mode" != $UPDATE ] && [ "$mode" != '' ] && echo "Invalid mode passed in. Use 'create' or 'update'"
}

function validateDestEnv()
{
  env=${1,,}
  [ "$env" != $ENV_DEV ] && [ "$env" != $ENV_TEST ] && [ "$env" != $ENV_PROD ] && echo "Invalid dest-env passed in. Use 'dev', 'test' or 'prod'" && exit
}