#!/bin/bash
######################################################################################################
# Generate JWT
#
# $1 - JWT secret
######################################################################################################
function generateJWT()
{
	epoc=$(date +%s)
	expiration=$(date -d "+ 500 minutes" +%s)

	header='{"alg":"HS256","typ":"JWT"}'
	payload="{\"iss\":\"Location Bash\",\"aud\":\"onboarding\",\"iat\":$epoc,\"exp\":$expiration}"

	jwt=$(jwt "$header" "$payload" "$1")

	# Remove the word 'JWT Token: ' returned from 'jwt' command
	jwt="${jwt:23}"

	echo $jwt
}