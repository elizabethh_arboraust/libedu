#!/bin/bash
######################################################################################################
# Check if parameter $1 is a positive integer
#
# $1 - The input to be inspected
#
# Returns 0 if positive integer, else 1
######################################################################################################
function isPositiveInteger()
{
	if [ $1 -ge 0 2> /dev/null ]
	then
		echo 0
	else
		echo 1
	fi
}