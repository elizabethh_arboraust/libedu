#!/bin/bash
######################################################################################################
# Write the error to usual log in storage/logs/ and display in console
######################################################################################################
function writeAndDisplayError()
{
	date=$(date '+%Y-%m-%d')
	storage="/home/appuser/onboarding/storage/logs/laravel-$date.log"

	dateTime=$(date '+%Y-%m-%d %H:%M:%S')

	echo "$1"
	echo "$2"

	# write the list of users with error to standard error log
  {
    echo "[$dateTime] $1"
    echo "$2"
    echo ""
  } >> "$storage"
}

######################################################################################################
# This functions expect php to return errorCode "0" when there's no error
#
# Example result returned from Controller inheriting BashController class and using getResult():
#   { "errorCode": "0", "errorMessage": null, "result": null, "httpStatusCode": 200, "response": null }
#
# $1 - errorCode
# $2 - error message
# $3 - any error related results
#
######################################################################################################
function quit()
{
	if [ "$1" != "0" ]
	then
		writeAndDisplayError "$2" "$3"
		exit
	fi
}