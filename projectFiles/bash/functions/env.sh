#!/bin/bash
######################################################################################################
# Get DB Config from .env file
# envFile location can be overridden by passing it as the first argument when including this script
######################################################################################################

function getEnvVar()
{
  value=$(grep -w $1 "$2" | cut -d '=' -f2 | tr -d "\r")
  first="$(echo $value | head -c 1)"
  last="${value: -1}"
  [ $first == \" ] && [ $last == \" ] && echo "${value:1:-1}" || echo $value
}