#!/bin/bash
######################################################################################################
# Get DB Config from .env file
# envFile location can be overridden by passing it as the first argument when including this script
######################################################################################################

bashDir="$destDir/bash"
functionDir="$bashDir/functions"
configDir="$bashDir/config"

source "$functionDir/args.sh"
source "$functionDir/env.sh"
source "$functionDir/jwt.sh"
source "$functionDir/logging.sh"
source "$functionDir/number.sh"
source "$functionDir/string.sh"

source "$configDir/constants.sh"
source "$configDir/env.sh"
source "$configDir/var.sh"
source "$configDir/dbItems.sh"