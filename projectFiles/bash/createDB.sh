#!/bin/bash
#################################################################################
# This script will:
# 1. Create a database
# 2. Create a new user that will only have access to onboarding database
# 3. Execute database scripts
#################################################################################

host=$DB_HOST
root=root
db=$DB_DATABASE
newUser=$DB_USERNAME
newUserPassword=$DB_PASSWORD
port=$DB_PORT
phpmyadmin=phpmyadmin

project=libEdu
onboardingDir="$destDir/$project"
sqlscriptsDir="$onboardingDir/sqlscripts"
tablesDir="$sqlscriptsDir/tables"

dropDB=
dropUser=

if [[ $* == *"--test"* ]]
then
  dropDB="DROP DATABASE IF EXISTS $db;"
  dropUser="DROP USER IF EXISTS $newUser@$host;"
fi

echo "Creating DB.."

# Windows
#sudo mysql --host=$host --user=$root -p --execute="
#Ubuntu
sudo mysql --execute="
$dropDB

USE moodle;
CREATE USER IF NOT EXISTS $newUser@$host IDENTIFIED BY '.7g+{UQGy@;nJE:,';
GRANT SELECT, INSERT, UPDATE, DELETE ON moodle.* TO $newUser@$host;

FLUSH PRIVILEGES;

CREATE DATABASE $db;

USE $db;

FLUSH PRIVILEGES;

$dropUser

CREATE USER IF NOT EXISTS $newUser@$host IDENTIFIED BY '$newUserPassword';
GRANT SELECT, INSERT, UPDATE, DELETE ON $db.* TO $newUser@$host;

CREATE USER IF NOT EXISTS $phpmyadmin@$host IDENTIFIED BY 'jCgn6qmbeqhM2u3Y';
GRANT SELECT, INSERT, UPDATE, DELETE ON $db.* TO $phpmyadmin@$host;

FLUSH PRIVILEGES;

$dbItems
" || exit

cd "$onboardingDir"
#php artisan db:seed

test=$(compareStrings "$APP_ENV", "$TEST")

if [[ "$test" -ne "1" ]]
then
  #source "$bashDir/testPatches.sh"
  echo ''
fi
