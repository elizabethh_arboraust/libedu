#!/bin/bash
######################################################################################################
# Constants
######################################################################################################

# Modes
CREATE=create
UPDATE=update
CREATE_SHORT=c
UPDATE_SHORT=u

# Env
ENV_DEV=dev
ENV_TEST=test
ENV_PROD=prod

# App env
TEST=test
ROOT=root