#!/bin/bash
######################################################################################################
# Get DB Config from .env file
# envFile location can be overridden by passing it as the first argument when including this script
######################################################################################################

project=libEdu
envFile="$destDir/$project/.env"

APP_ENV=$(getEnvVar APP_ENV "$envFile")

DB_HOST=$(getEnvVar DB_HOST "$envFile")
DB_DATABASE=$(getEnvVar DB_DATABASE "$envFile")
DB_USERNAME=$(getEnvVar DB_USERNAME "$envFile")
DB_PASSWORD=$(getEnvVar DB_PASSWORD "$envFile")
DB_PORT=$(getEnvVar DB_PORT "$envFile")