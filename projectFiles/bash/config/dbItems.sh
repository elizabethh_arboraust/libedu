#!/bin/bash
######################################################################################################
# Constants
######################################################################################################

project=libEdu
onboardingDir="$destDir/$project"
sqlscriptsDir="$onboardingDir/sqlscripts"

tablesDir="$sqlscriptsDir/tables"
patchesDir="$sqlscriptsDir/patches"
spDir="$sqlscriptsDir/storedProcedures"

patchDir="$tablesDir/Patch"

dbItems="
# create TABLES
source $tablesDir/broadcasts.sql
source $tablesDir/users.sql
"
