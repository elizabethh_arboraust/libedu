#!/bin/bash
#################################################################################
# This script will auto-install or auto-update depending if the destination folder exists or not.
# If destination folder doesn't exist, this script copy files and setup database.
# If destination folder exists, this script will copy updates and run any sql update.
#
# To force create (first time), pass in 'create' as the parameter e.g. bash deployProject.sh create
# To force update, pass in 'update' as the parameter e.g. bash deployProject.sh update
#################################################################################

: "echo '
  Usage: bash deployProject.sh --mode=create [--tag=tag-test --prev-tag=VM0.0.1] [--branch=branch-test --prev-branch=test0.0.1] [--test]
  --mode                    create or update
  --tag/--prev-tag          these should be used together. Cannot be used together with --branch/--prev-branch
  --branch/--prev-branch    these should be used together. Cannot be used together with --tag/--prev-tag
  --test                    when not in linux system, this will create appuser folder in a different path
  '"

echo "
Usage: sudo bash deployProject.sh
"

project=libEdu
projectFilesDir="projectFiles"
destDir="/var/www/sso"
destDirTest="../../appuser"
user=mls01

if [[ $* == *"--local"* ]]
then
  destDir=$destDirTest
fi

destOnboardingDir="$destDir/$project"
webuser="www-data"

bashDir="$destDir/bash"
includeDir="$bashDir/include"
storage="$destOnboardingDir/storage"

echo '(u)pdate or (c)reate?'
read mode
mode=${mode,,}

echo "Copying projectFiles folder.."
sudo rsync -rztD --stats --info=progress2 --human-readable "$projectFilesDir/" "$destDir/"

source "$includeDir/include.sh"

# Add provisioning and appuser to www-data group
sudo usermod -a -G $webuser $user

#set folder permission
# read(+execute)/write to
# - storage/
# read(+execute) only for the rest and bash/ folder
sudo chown -R $webuser:$webuser $destOnboardingDir
sudo chown -R $webuser:$webuser $bashDir
sudo chmod -R 770 $destOnboardingDir
sudo chmod -R 770 $bashDir
sudo chmod -R 770 "$destOnboardingDir/public"
sudo chmod -R 770 "$storage/"
sudo chmod -R 770 "$destOnboardingDir/bootstrap/cache"

# set permission for newly created files
sudo chmod g+s $destOnboardingDir
sudo chmod g+s $bashDir
sudo chmod g+s "$destOnboardingDir/public"
sudo chmod g+s "$storage/"
sudo chmod g+s "$destOnboardingDir/bootstrap/cache"

if [ "$mode" = "$CREATE" ] || [ "$mode" = "$CREATE_SHORT" ]
then
  source "$bashDir/createDB.sh"

else

  echo "Applying patches.."

  mysql -uroot --execute="

  USE $DB_DATABASE;

  $dbItems
  " || exit

  echo ''
  echo "Updated!"
  echo ''
fi

# restart apache
sudo service apache2 restart